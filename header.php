<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<link rel="profile" href="https://gmpg.org/xfn/11" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php do_action( 'artstation_body' ); ?>

	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'artstation' ); ?></a>

		<header id="masthead">
			<?php get_sidebar( 'alerts' ); ?>

			<nav></nav>
		</header> <!-- #masthead -->

		<div id="content" class="site-content">
