# Changelog

## 0.1.0

**Added:**

1. Theme mods sync between parent and child.
2. Modular enqueue example for 'admin_enqueue' hook.
3. Language loading.
4. Customizer settings.
5. Disabled Gutenberg for now.
6. Sidebar registration – alerts, main and footer columns.
7. Basic header, index and footer templates.
