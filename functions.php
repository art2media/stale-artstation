<?php

// Defaults for theme_mods.
$theme_mods = get_theme_mods();
$mods       = array();

$mods['footer_widgets'] = ( empty( $theme_mods['footer_widgets'] ) ) ? 0 : (int) $theme_mods['footer_widgets'];

$mods['block_editor_enable'] = ( empty( $theme_mods['block_editor_enable'] ) ) ? false : $theme_mods['block_editor_enable'];

// Defaults for multisite.
if ( is_multisite() ) {
	$blog_details = get_blog_details();

	$mu = array();
} else {
	$mu = array();
}

// Constants are slow. Always buffer the values in variables.
define( 'ARTSTATION', array(
	'wp_site_url'   => get_site_url(),
	'customize_url' => wp_customize_url(),
	'dir'           => get_template_directory(),
	'uri'           => get_template_directory_uri(),
	'mods'          => $mods,
	'mu'            => $mu,
) );

$includes_dir = ARTSTATION['dir'] . '/includes';

// Theme Setup
require_once $includes_dir . '/setup_theme.php';
add_action( 'after_setup_theme', 'artstation_setup_theme' );

// Switch Theme
require_once $includes_dir . '/switch_theme.php';
add_action( 'switch_theme', 'artstation_switch_theme' );
add_action( 'after_switch_theme', 'artstation_after_switch_theme', 10, 2 );

// Updates
require_once $includes_dir . '/updates.php';

// Sidebars
require_once $includes_dir . '/sidebars.php';
add_action( 'widgets_init', 'artstation_register_sidebars' );

// Customize
require_once $includes_dir . '/customize.php';
add_action( 'customize_register', 'artstation_customize_register' );

// Admin Enqueue
require_once $includes_dir . '/admin_enqueue.php';
add_action( 'admin_enqueue_scripts', 'artstation_admin_enqueue' );

// Body
require_once $includes_dir . '/body.php';
add_action( 'artstation_body', 'artstation_body' );

// Gutenberg
$block_editor_enable = ( ARTSTATION['mods']['block_editor_enable'] ) ? '__return_true' : '__return_false';
add_filter( 'use_block_editor_for_post', $block_editor_enable );
