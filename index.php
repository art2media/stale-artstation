<?php get_header(); ?>

	<main id="primary" class="site-main" role="main">

		<?php
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
			}
		} else {
		} ?>

	</main> <!-- #primary -->

	<?php get_sidebar( 'main' ); ?>

<?php get_footer(); ?>
