<?php
if ( ! is_active_sidebar( 'alerts' ) ) {
	return;
} ?>

<div class="alerts">

	<?php dynamic_sidebar( 'alerts' ); ?>

</div>
