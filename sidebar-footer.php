<?php
$footer_widgets = ARTSTATION['mods']['footer_widgets'];

if ( ! empty( $footer_widgets ) ) : ?>

	<footer id="footer-widgets">

	<?php
	for ( $i = 1; $i <= $footer_widgets; $i++ ) :
		if ( 1 === $i ) {
			$sidebar = 'footer';
		} else {
			$sidebar = 'footer-' . $i;
		}

		if ( ! is_active_sidebar( $sidebar ) ) {
			continue;
		} ?>

		<aside class="widget-area column-<?php echo $i; ?>">
			<ul class="widgets">

				<?php dynamic_sidebar( $sidebar ); ?>

			</ul>
		</aside>

	<?php endfor; ?>

	</footer>

<?php endif; ?>
