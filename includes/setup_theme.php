<?php

function artstation_setup_theme() {
	$template_dir = ARTSTATION['dir'];

	load_theme_textdomain( 'artstation', $template_dir . '/languages' );
}
