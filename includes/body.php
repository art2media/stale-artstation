<?php

// Add code after <body> tag.
function artstation_body() {
	// Add GTM noscript tag if gtm4wp is active.
	if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) {
		echo gtm4wp_the_gtm_tag();
	}
}
