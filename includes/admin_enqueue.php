<?php

function artstation_admin_enqueue() {
	$build_status = ARTSTATION['mods']['build_status'];
	$template_uri = ARTSTATION['uri'];
	$screen       = get_current_screen();
	$localize     = array(
		'ajax_url' => admin_url( 'admin-ajax.php' ),
	);

	wp_enqueue_style( 'artstation_admin_global', $template_uri . '/css/admin-global.css' );

	wp_enqueue_script( 'artstation_admin_global', $template_uri . '/js/admin-global.js', array( 'jquery' ), '', true );
	wp_localize_script( 'artstation_admin_global', 'artstation', $localize );
}
