<?php

// Theme is being deactivated.
function artstation_switch_theme() {
	if ( ! is_child_theme() ) {
		$mods       = ARTSTATION['mods'];
		$theme      = wp_get_theme();
		$template   = $theme->get( 'Template' );
		$stylesheet = get_stylesheet();

		// Sync theme mods only from/to child theme.
		if ( 'artstation' === $template ) {
			update_option( 'theme_mods_' . $stylesheet, $mods );
		}
	}
};

// Theme is being activated.
function artstation_after_switch_theme( $old_name, $old_theme ) {
	if ( ! is_child_theme() ) {
		$old_mods   = get_option( 'theme_mods_' . $old_theme->stylesheet );
		$stylesheet = get_stylesheet();
		$template   = $old_theme->get( 'Template' );

		// Sync theme mods only from/to child theme.
		if ( 'artstation' === $template ) {
			update_option( 'theme_mods_' . $stylesheet, $old_mods );
		}
	}
};
