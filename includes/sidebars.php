<?php

// Register dynamic sidebars.
function artstation_register_sidebars() {
	$footer_widgets = ARTSTATION['mods']['footer_widgets'];

	// Alerts sidebar.
	register_sidebar( array(
		'name'        => esc_html__( 'Alerts', 'artstation' ),
		'id'          => 'alerts',
		'description' => esc_html__( 'Notifies users of important events.', 'artstation' ),
	) );

	// Main sidebar.
	register_sidebar( array(
		'name'        => esc_html__( 'Sidebar', 'artstation' ),
		'id'          => 'main',
		'description' => esc_html__( 'Displays widgets on all posts and pages.', 'artstation' ),
	) );

	// Footer sidebars.
	if ( ! empty( $footer_widgets ) ) {
		if ( 1 === $footer_widgets ) {
			$name = esc_html__( 'Footer', 'artstation' );
		} else {
			$name = esc_html__( 'Footer %d', 'artstation' );
		}

		// Register multiple sidebars at once.
		register_sidebars( $footer_widgets, array(
			'name' => $name,
			'id'   => 'footer',
		) );
	}
}
