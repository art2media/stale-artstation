<?php

$template_dir = ARTSTATION['dir'];

require_once $template_dir . '/updates/plugin-update-checker.php';

$update_checker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/art2media/artstation',
	$template_dir,
	'artstation'
);
