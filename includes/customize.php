<?php

function artstation_customize_register( $wp_customize ) {
	// Sections
	$wp_customize->add_section( 'artstation_theme', array(
		'title' => esc_html__( 'artstation', 'artstation' ),
	) );

	// Settings
	$wp_customize->add_setting( 'block_editor_enable', array(
		'default'           => false,
		'sanitize_callback' => 'artstation_sanitize_checkbox',
	) );

	$wp_customize->add_setting( 'footer_widgets', array(
		'default'           => '0',
		'sanitize_callback' => 'artstation_sanitize_select',
	) );

	// Controls
	$wp_customize->add_control( 'block_editor_enable', array(
		'label'    => esc_html__( 'Enable Gutenberg', 'artstation' ),
		'section'  => 'artstation_theme',
		'settings' => 'block_editor_enable',
		'type'     => 'checkbox',
	) );

	$wp_customize->add_control( 'footer_widgets', array(
		'label'    => esc_html__( 'Footer Widgets', 'artstation' ),
		'section'  => 'artstation_theme',
		'settings' => 'footer_widgets',
		'type'     => 'select',
		'choices'  => array(
			'0' => esc_html__( 'No Widgets', 'artstation' ),
			'1' => esc_html__( 'One Column', 'artstation' ),
			'2' => esc_html__( 'Two Columns', 'artstation' ),
			'3' => esc_html__( 'Three Columns', 'artstation' ),
			'4' => esc_html__( 'Four Columns', 'artstation' ),
		),
	) );
}

// Sanitization
function artstation_sanitize_checkbox( $input ) {
	// Return true if checkbox is checked.
	return ( isset( $input ) && true === $input ) ? true : false;
}

function artstation_sanitize_select( $input, $setting ) {
	// Input must be a slug: lowercase alphanumeric characters, dashes and underscores are allowed
	$input = sanitize_key( $input );

	// Get a list of possible select options.
	$choices = $setting->manager->get_control( $setting->id )->choices;

	// Return input if valid or return default option.
	return ( array_key_exists( $input, $choices ) ) ? $input : $setting->default;
}
