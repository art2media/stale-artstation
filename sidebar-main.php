<?php
if ( ! is_active_sidebar( 'main' ) ) {
	return;
} ?>

<aside id="secondary" class="widget-area" role="complementary">

	<?php dynamic_sidebar( 'main' ); ?>

</aside> <!-- #secondary -->
